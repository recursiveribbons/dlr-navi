# Dokumentation für Endanwender/Betreuer

Die **DLRNaviApp** hilft bei der genaueren Geo-Lokalsierung im Gelände für die Aufnahme von Messwerten.


## Allgemeine Informationen


#### Wo kommen die Daten fürs Overview her?

Die angezeigten Lokationsdaten werden durch Android in Form von geographischen Koordinaten (über WGS 84) bereitgestellt und durch die App in UTM-Koordinaten umgewandelt.



#### Was passiert, wenn ich den QR-Code **eines** Beacons scanne?

Der QR-Code des Beacons enthält die eingemessenen Positionsdaten des Beacons in UTM-Koordinaten.
Aus den GPS-Daten des Endgerätes, welche in UTM-Koordinaten umgewandelt werden, und den Positionsdaten des Beacons wird eine Differenz für Easting und Northing berechnet.

Die jeweilige Offset für Easting und Northing wird gespeichert und beim weiteren Bewegen des Endgerätes auf die vom Endgerät entnommenen GPS-Daten hinzugerechnet.

Der Offset in Metern wird entsprechend dem Satz des Pythagoras berechnet und dem Benutzer angezeigt.

Die _Time since reference_ und die _Distance to reference_ zeigen dem Benutzer an, wieviel Zeit vergangen ist seit der letzten Kalibrierung bzw. wie groß die Entfernung zu diesem letzten Referenzpunkt ist.


#### Was passiert, wenn ich eine Liste von QR-Beacons scanne?

Durch Scannen einer Liste von Beacon-Informationen werden diese dem Benutzer auf der Karte dargestellt.
Die App prüft selbstständig, ob gescannte Beacons bereits vorhanden sind und fügt nur ggf. neue hinzu.

#### Wie kann ich einzelne QR-Beacons von der Karte entfernen?

Die Liste aller gespeicherten Beacons kann gelöscht werden, indem ein entsprechender _Clear_-QR-Code gescannt wird.

* { "clear":true }


#### Wie müssen die QR-Beacon Informationen für einen einzelnen/eine Liste kodiert sein? (Optionale Attribute o.Ä.)

Die QR-Codes für einzelne Beacons (zur Kalibrierung) werden als JSON-_Objekte_ kodiert.
Optional ist hierbei die id-Attribut.

Bsp.:
* { longzone:32, latzone:"U", easting:"681751.443163194", northing:"5644584.71786718", id:"swep-paradies-DGPS-UTM-1" }

Die QR-Codes für eine Liste von Beacons zur Darstellung auf der Karte werden als JSON-Objekte innerhalb eines JSON-_Array_ gespeichert.
Der QR-Scanner der App unterscheidet selbstständig zwischen Referenzkalibrierung und Einlesen von Kartenbeacons.

Bsp.:  
[{ longzone:32, latzone:"U", easting:"681751.443163194", northing:"5644584.71786718", id:"swep-paradies-DGPS-UTM-1" },  
{ longzone:32, latzone:"U", easting:"681776.437387749", northing:"5644555.88479576", id:"swep-paradies-DGPS-UTM-2" },  
{ longzone:32, latzone:"U", easting:"681715.453141833", northing:"5644528.96907906", id:"swep-paradies-DGPS-UTM-3" },  
{ longzone:32, latzone:"U", easting:"681632.893823612", northing:"5644435.63228559", id:"swep-paradies-DGPS-UTM-4" }]



#### Welcher Farbmarker auf der Karte steht für was?

Der blaue Marker ist die mit Offset korrigierte Position, und der rote Marker ist die rohe Position aus dem GPS-Messung.

#### Was genau bedeutet die angezeigte Accuracy?

Die Accuracy wird von Android aus den Daten für den GPS-Fix entnommen.

Der angezeigte Wert entspricht einem Radius mit einem Vertrauensintervall von 68%, d.h. mit einer Wahrscheinlichkeit von 68% ist die wahre Position des Benutzers in einem Radius des angezeigten Wertes.

## Fehlerbehebung

#### Was mache ich, wenn "waiting for signal" dauerhaft angezeigt wird?

Dieser Fehler tritt auf, wenn das Endgerät kein GPS-Daten erhalten kann.

Mögliche Ursachen:
* GPS ist deaktiviert im Gerät
* Die Verbindung zu Satelliten ist beeinträchtigt (z.B. in Gebäuden oder im Gelände durch Baumkronen o.Ä.)
* Es sind nicht genügend Satelliten erreichbar, um einen GPS-Fix zu ermitteln