package com.sillywalks.api;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.sillywalks.api.types.Beacon;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class BeaconManagerTest {
    private BeaconManager beaconManager;
    private Context context;

    @Before
    public void createBeaconManager() {
        context = InstrumentationRegistry.getTargetContext();
        assertEquals("com.sillywalks.naviapp", context.getPackageName());
        beaconManager = BeaconManager.getInstance(context);
    }

    @Test
    public void testBeacons() {
        List<Beacon> beacons = new LinkedList<>();
        Random random = new Random();
        random.nextDouble();
        for(int i=0; i<10; i++) {
            beacons.add(new Beacon("test" + i, new Coordinates(random.nextDouble() * 50, random.nextDouble() * 50)));
        }
        Beacon test = new Beacon("test", new Coordinates(random.nextDouble() * 50, random.nextDouble() * 50));
        beacons.add(test);
        Comparator<Beacon> idCompare = new Comparator<Beacon>() {
            @Override
            public int compare(Beacon beacon, Beacon t1) {
                return beacon.getBeaconId().compareTo(t1.getBeaconId());
            }
        };
        beacons.sort(idCompare);
        beaconManager.refreshBeacons(beacons);
        List<Beacon> list = beaconManager.getBeacons();
        assertArrayEquals(beacons.toArray(), list.toArray());
        Beacon result = beaconManager.getBeacon(test.getBeaconId());
        assertEquals(test, result);
        beaconManager.refreshBeacons(new LinkedList<Beacon>());
        beacons.clear();
        list = beaconManager.getBeacons();
        assertArrayEquals(beacons.toArray(), list.toArray());
    }

    @After
    public void clearBeacons() {
        beaconManager.refreshBeacons(new LinkedList<Beacon>());
    }
}