package com.sillywalks.api;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.sillywalks.api.types.Note;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class NoteManagerTest {
    private NoteManager noteManager;

    @Before
    public void createNoteManager() {
        Context context = InstrumentationRegistry.getTargetContext();
        assertEquals("com.sillywalks.naviapp", context.getPackageName());
        noteManager = new NoteManager(context);
    }

    @Test
    public void saveNote() {
        Random random = new Random();
        Note test1 = new Note(new Coordinates(random.nextDouble() * 50, random.nextDouble() * 50), "test1");
        Note note1 = noteManager.saveNote(test1);
        assertEquals(test1.getPosition(), note1.getPosition());
        assertEquals(test1.getMessage(), note1.getMessage());
        test1.getTimestamp().set(GregorianCalendar.MILLISECOND, 0);
        assertEquals(test1.getTimestamp(), note1.getTimestamp());
        Note result1 = noteManager.getNote(note1.getId());
        assertEquals(note1, result1);
    }

    @Test
    public void getNotes() {
        Random random = new Random();
        List<Note> notes = new LinkedList<>();
        for(int i=0; i<10; i++) {
            Note n = new Note(new Coordinates(random.nextDouble() * 50, random.nextDouble() * 50), "test" + i);
            Note n1 = noteManager.saveNote(n);
            assertEquals(n.getPosition(), n1.getPosition());
            assertEquals(n.getMessage(), n1.getMessage());
            notes.add(n1);
        }
        List<Note> resultList = noteManager.getNotes();
        assertArrayEquals(notes.toArray(), resultList.toArray());
        Note[] resultArray = noteManager.getNotesArray();
        assertArrayEquals(notes.toArray(), resultArray);
    }

    @After
    public void clearNotes() {
        noteManager.clearNotes();
    }
}
