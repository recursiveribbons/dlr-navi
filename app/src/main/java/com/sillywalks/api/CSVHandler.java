package com.sillywalks.api;

import android.content.Context;

import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import com.sillywalks.api.types.Note;

import java.io.StringWriter;
import java.util.List;

/**
 * This class converts Notes to CSV format.
 */
public class CSVHandler {
    private NoteManager noteManager;

    /**
     * Constructor with Context
     * @param context Android Context
     */
    public CSVHandler(Context context) {
        noteManager = new NoteManager(context);
    }

    /**
     * Converts all Notes in the DB as a CSV string.
     * @return CSV of Notes as String
     */
    public String exportNotes() {
        List<Note> notes = noteManager.getNotes();
        StringWriter writer = new StringWriter();
        ICSVWriter csvWriter = new CSVWriterBuilder(writer).build();
        csvWriter.writeNext(Note.getFieldsStringArray());
        for (Note note : notes) {
            csvWriter.writeNext(note.toStringArray());
        }
        return writer.toString();
    }
}
