package com.sillywalks.api;

import android.util.Log;

import com.sillywalks.api.types.Offset;
import com.sillywalks.api.types.UTMCoordinates;

import java.util.Calendar;
import java.util.Observable;

/**
 * An Observable that contains information
 */
public class CurrentMeasurements extends Observable {

    private static final String TAG = "DLR";

    private UTMCoordinates correctedLocation;
    private UTMCoordinates phoneGpsLocation;
    private Offset offset;
    private UTMCoordinates lastScannedBeacon;

    CurrentMeasurements() {
        this.offset = new Offset(0,0);
    }


    /**
     * Sets the location measured by phone GPS
     * @param phoneCoordinates Location provided by phone GPS
     */
    public void setPhoneGpsMeasurements(UTMCoordinates phoneCoordinates) {
        phoneGpsLocation = phoneCoordinates;
        correctedLocation = phoneGpsLocation.addOffsetAndReturnCoordinates(offset);
        setChanged();
        notifyObservers();
    }

    // current = location calculated using qr beacon offset
    public void setCorrectedMeasurements(UTMCoordinates scannedBeaconLocation, UTMCoordinates phoneGpsLocation, Offset offset) {
        this.correctedLocation = scannedBeaconLocation;
        this.lastScannedBeacon = scannedBeaconLocation;
        this.phoneGpsLocation = phoneGpsLocation;
        this.offset = offset;

        logMeasuredAndCurrentLocation();
        setChanged();
        notifyObservers();
    }

    private void logMeasuredAndCurrentLocation() {
        Log.d(TAG,"phoneGpsLocation ("
                + phoneGpsLocation.getEasting() + ","
                + phoneGpsLocation.getNorthing() + ") - correctedLocation ("
                + correctedLocation.getEasting() + ","
                + correctedLocation.getEasting() + "), - offset ("
                + offset.getEasting() + ","
                + offset.getNorthing() + ")");
    }


    // location including possible offset
    public UTMCoordinates getCorrectedLocation() {
        return correctedLocation;
    }

    // location provided by phone GPS
    public UTMCoordinates getPhoneGpsLocation() {
        return phoneGpsLocation;
    }


    public int getNumberOfSatellitesOfGPSFix() {
        //Log.d(TAG, "CurrentMeasurements.getNumberOfSatellitesOfGPSFix(): " + phoneGpsLocation.getSatellitesUsedForFix());
        return phoneGpsLocation.getSatellitesUsedForFix();
    }

    public double getAccuracyOfGPSFix() {
        return phoneGpsLocation.getAccuracy();
    }


    public double getOffsetInMeters() {
        return offset.getInMeters();
    }

    public Double getDistanceToLastBeaconInMeters() {
        Double distance = null;
        if (lastScannedBeacon != null) {
            distance = correctedLocation.getDistanceTo(lastScannedBeacon);
        }
        return distance;
    }


    public Calendar getTimeOfLastCalibration() {
        if (lastScannedBeacon != null) {
            return lastScannedBeacon.getTimestamp();
        }
        return null;
    }


}
