package com.sillywalks.api;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.sillywalks.api.db.DatabaseHelper;
import com.sillywalks.api.types.Beacon;
import com.sillywalks.api.types.Offset;
import com.sillywalks.api.types.UTMCoordinates;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Observer;

/**
 * This class keeps track of the current GPS location and handles offset calculation.
 */
public class GPSCorrector {
    private LocationManager locationManager;
    private GPSLocationListener gpsLocationListener;
    private BeaconManager beaconManager;
    private CurrentMeasurements currentMeasurements;
    private RescanStatus rescanStatus;

    private static final String TAG = "DLR";

    private static final long MIN_TIME_IN_MS = 1000;  // update every 1s
    private static final float MIN_DISTANCE_IN_METERS = 0;  // update irregardless of meter change


    private static GPSCorrector instance;

    /**
     * Gets a singleton instance of GPSCorrector
     * @param context Android Context
     * @return A Singleton instance of GPSCorrector
     */
    public static GPSCorrector getInstance(Context context) {
        if(instance == null) {
            instance = new GPSCorrector(context);
        }
        return instance;
    }

    /**
     * Private constructor for Singleton. Use {@link #getInstance(Context) getInstance} instead.
     * @param context Application context
     */
    private GPSCorrector(Context context) {
        beaconManager = BeaconManager.getInstance(context);

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException("Does not have permission to access location");
        }

        currentMeasurements = new CurrentMeasurements();
        rescanStatus = new RescanStatus();
        startLocationUpdates();
    }

    /**
     * Starts the location listener
     */
    public void startLocationUpdates() {
        if (gpsLocationListener == null) {
            gpsLocationListener = new GPSLocationListener();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    MIN_TIME_IN_MS,
                    MIN_DISTANCE_IN_METERS,
                    gpsLocationListener);

            Log.d(TAG, "GPSCorrector.startLocationUpdates() called");
        }
    }

    /**
     * Stops the location listener
     */
    public void stopLocationUpdates() {
        if (gpsLocationListener != null) {
            locationManager.removeUpdates(gpsLocationListener);
            Log.d(TAG, "GPSCorrector.stopLocationUpdates() called");

            gpsLocationListener = null;
        }
    }

    /**
     * Sets the current scannedBeacon position
     * @param scannedBeacon Position scanned from QR or beacon
     */
    public boolean setKnownPosition(UTMCoordinates scannedBeacon) {
        UTMCoordinates phoneGpsUTM = currentMeasurements.getPhoneGpsLocation();
        if(phoneGpsUTM == null) {
            return false;
        }
        Offset delta = new Offset(scannedBeacon, phoneGpsUTM);
        currentMeasurements.setCorrectedMeasurements(scannedBeacon, phoneGpsUTM, delta);
        return true;
    }

    /**
     * Sets the current known position
     * @param beaconId Beacon ID
     */
    public boolean setKnownPosition(String beaconId) {
        Beacon beacon = beaconManager.getBeacon(beaconId);
        if(beacon == null) {
            return false;
        }
        UTMCoordinates known = beacon.getLocation();
        setKnownPosition(known);
        return true;
    }

    /**
     * Subscribe to the {@link com.sillywalks.api.CurrentMeasurements CurrentMeasurements} Observable, which includes corrected position , measured position, and offset.
     * @param observer Object with update method
     */
    public void subscribeToCurrentMeasurements(Observer observer) {
        currentMeasurements.addObserver(observer);
    }

    /**
     * Subscribe to the {@link com.sillywalks.api.RescanStatus RescanStatus} Observable, which includes
     * @param observer Object with update method
     */
    public void subscribeToRescanStatus(Observer observer) {
        rescanStatus.addObserver(observer);
    }

    /**
     * Returns the current position after correcting for offset
     * @return offset-corrected position
     * @deprecated Subscribe to {@link com.sillywalks.api.CurrentMeasurements CurrentMeasurements} instead
     */
    @Deprecated
    public UTMCoordinates getCorrectedPosition() {
        return currentMeasurements.getCorrectedLocation();
    }

    /**
     * Listener for GPS location.
     */
    private class GPSLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                CoordinateConversion conversion = new CoordinateConversion();
                UTMCoordinates locationUTM = conversion.location2UTMCoordinates(location);

                if (location.getExtras().containsKey("satellites")) {
                    locationUTM.setSatellitesUsedForFix(location.getExtras().getInt("satellites"));
                }

                locationUTM.setAccuracy(location.getAccuracy());
                locationUTM.setAltitude(location.getAltitude());
                currentMeasurements.setPhoneGpsMeasurements(locationUTM);
                rescanStatus.setMeasurements(locationUTM);
            }
            else {
                Log.e(TAG, "GPSCorrector - LocationListener is null");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "GPSCorrector - " + provider
                    + " provider status changed: "
                    + status);

        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "GPSCorrector - " + provider + " provider enabled");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "GPSCorrector - " + provider + " provider disabled");
        }
    }
}
