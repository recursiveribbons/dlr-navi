package com.sillywalks.api;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sillywalks.api.types.Note;
import com.sillywalks.api.db.DatabaseHelper;
import com.sillywalks.api.types.UTMCoordinates;

import java.util.LinkedList;
import java.util.List;

public class NoteManager {
    private DatabaseHelper dbHelper;

    /**
     * The fields in the note table in the database.
     */
    private final String[] fields = new String[]{"_id",
            "timestamp",
            "longzone",
            "latzone",
            "easting",
            "northing",
            "message",
            "altitude",
            "accuracy",
            "satellites"};

    /**
     * Constructor with Context. The Context is required to initialise the DatabaseHelper.
     * @param context Android app context
     */
    public NoteManager(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    /**
     * Gets a Note from the DB based on the Note ID.
     * @param id ID of Note
     * @return Note if ID is found, otherwise null
     */
    public Note getNote(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor result = db.query("notes", fields, "_id=?", new String[]{String.valueOf(id)}, null, null, null);
        Note n;
        if(result.moveToFirst()) {
            n = new Note(id,
                    result.getString(1),
                    new UTMCoordinates(result.getInt(2), result.getString(3), result.getDouble(4), result.getDouble(5)),
                    result.getString(6));
        } else {
            n = null;
        }
        result.close();
        db.close();
        return n;
    }

    /**
     * Gets a list of all notes in the DB.
     * @return List of Notes
     */
    public List<Note> getNotes() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor result = db.query("notes", fields, null, null, null, null, "_id");
        List<Note> notes = new LinkedList<>();
        while (result.moveToNext()) {
            notes.add(new Note(result.getInt(0),
                    result.getString(1),
                    new UTMCoordinates(result.getInt(2), result.getString(3), result.getDouble(4), result.getDouble(5)),
                    result.getString(6)));
        }
        result.close();
        db.close();
        return notes;
    }

    /**
     * Save a Note to the DB. The Note object does not require an ID field, as it is generated by the DB.
     * @param n Note to save
     * @return Saved note in DB. Contains the Note ID.
     */
    public Note saveNote(Note n) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int id = (int)db.insert("notes", null, n.toContentValues());
        return getNote(id);
    }

    /**
     * Clears all Notes from the DB.
     */
    public void clearNotes() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM notes");
        db.execSQL("VACUUM");
        db.close();
    }

    /**
     * Gets an array of all notes in the DB.
     * @return Array of Notes
     */
    public Note[] getNotesArray() {
        List<Note> notesList = getNotes();
        Note[] notesArray = new Note[notesList.size()];
        notesList.toArray(notesArray);
        return notesArray;
    }
}
