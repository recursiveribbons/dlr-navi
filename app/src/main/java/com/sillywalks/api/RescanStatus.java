package com.sillywalks.api;

import com.sillywalks.api.types.UTMCoordinates;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * This class determines whether a rescan at a beacon is required based on distance moved and satellite count.
 * If there was a sudden change in movement or satellite count, the rescan status is raised.
 * Register as an Observer to receive the current warning status.
 */
public class RescanStatus extends Observable {
    private static final int HISTORY_SIZE = 10;
    private static final int SAT_THRESHOLD = 3;
    private static final int DISTANCE_THRESHOLD = 3;

    private Status status = Status.RESCAN;
    private LinkedList<Integer> satellites;
    private LinkedList<Double> distancesMoved;
    private UTMCoordinates previous;

    RescanStatus() {
        satellites = new LinkedList<>();
        distancesMoved = new LinkedList<>();
        previous = null;
    }

    /**
     * Returns the current warning status
     * @return current warning status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the current status to the new status, only if the new status has a higher severity.
     * @param status The new status
     */
    private void raiseStatus(Status status) {
        if(status.isMoreSevereThan(this.status)) {
            this.status = status;
            setChanged();
            notifyObservers();
        }
    }

    /**
     * Resets the status to lowest severity, for example if a beacon has been scanned.
     */
    public void resetStatus() {
        status = Status.ACCURATE;
        setChanged();
        notifyObservers();
    }

    /**
     * Determines whether a rescan beacon warning should be raised,
     * based on a history of number of satellites and distance moved.
     * @param position latest measured position
     */
    void setMeasurements(UTMCoordinates position) {
        if(previous == null) {
            previous = position;
        }

        // Satellite count
        int numSat = position.getSatellitesUsedForFix();
        if (numSat > getAverage(satellites) + SAT_THRESHOLD) {
            raiseStatus(Status.WARNING);
            satellites.clear();
        }
        satellites.addLast(position.getSatellitesUsedForFix());
        if(satellites.size() > HISTORY_SIZE) {
            satellites.removeFirst();
        }

        // Distance change
        double distance = position.getDistanceTo(previous);
        if (distance > getAverage(distancesMoved) + DISTANCE_THRESHOLD) {
            raiseStatus(Status.RESCAN);
            distancesMoved.clear();
        }
        distancesMoved.addLast(distance);
        if(distancesMoved.size() > HISTORY_SIZE) {
            satellites.removeFirst();
        }
    }

    /**
     * Calculates the average in a List of numbers
     * @param list List of numbers
     * @return Average of numbers in list
     */
    private double getAverage(List<? extends Number> list) {
        double sum = 0;
        for (Number num : list) {
            sum += num.doubleValue();
        }
        return sum / satellites.size();
    }

    /**
     * The current rescan status. Shows how necessary a rescan currently is.
     */
    public enum Status {
        ACCURATE(0), WARNING(1), RESCAN(2);

        private final int level;

        Status(int level) {
            this.level = level;
        }

        /**
         * Determines whether a status is more severe than another status.
         * @param other Status to compare to
         * @return Whether status is more severe
         */
        public boolean isMoreSevereThan(Status other) {
            return this.level > other.level;
        }
    }
}
