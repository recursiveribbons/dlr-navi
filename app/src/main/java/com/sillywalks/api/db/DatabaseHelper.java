package com.sillywalks.api.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The interface between Android code and the SQLite DB
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "dlr-navi", null, 10);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE notes (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " longzone INTEGER," +
                " latzone TEXT," +
                " easting REAL," +
                " northing REAL," +
                " altitude REAL," +
                " accuracy REAL," +
                " satellites REAL," +
                " timestamp TEXT," +
                " message TEXT);");

        database.execSQL("CREATE TABLE location (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " timestamp TEXT," +

                " longzone_measured INTEGER," +
                " latzone_measured TEXT," +
                " easting_measured REAL," +
                " northing_measured REAL," +
                " altitude_measured REAL," +

                " longzone_corrected INTEGER," +
                " latzone_corrected TEXT," +
                " easting_corrected REAL," +
                " northing_corrected REAL," +
                " altitude_corrected REAL," +

                " current_offset REAL)");

        database.execSQL("CREATE TABLE beacons (_ID TEXT PRIMARY KEY," +
                " longzone INTEGER," +
                " latzone TEXT," +
                " easting REAL," +
                " northing REAL," +
                " altitude REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS notes");
        database.execSQL("DROP TABLE IF EXISTS location");
        database.execSQL("DROP TABLE IF EXISTS beacons");
        onCreate(database);
    }

    @Override
    public void onDowngrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS notes");
        database.execSQL("DROP TABLE IF EXISTS location");
        database.execSQL("DROP TABLE IF EXISTS beacons");
        onCreate(database);
    }
}
