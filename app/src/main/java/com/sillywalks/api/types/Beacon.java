package com.sillywalks.api.types;

import android.content.ContentValues;

/**
 * Represents a beacon
 */
public class Beacon {
    private String beaconId;
    private UTMCoordinates location;

    /**
     * Constructor with beacon ID and location
     * @param beaconId Beacon ID
     * @param location Beacon location
     */
    public Beacon(String beaconId, UTMCoordinates location) {
        this.beaconId = beaconId;
        this.location = location;
    }

    /**
     * Constructor with beacon location
     * @param location Beacon location
     */
    Beacon(UTMCoordinates location) {
        this.location = location;
    }

    /**
     * Gets the ID of the beacon
     * @return Beacon ID
     */
    public String getBeaconId() {
        return beaconId;
    }

    /**
     * Gets the location of the beacon
     * @return Beacon location
     */
    public UTMCoordinates getLocation() {
        return location;
    }

    /**
     * Converts the Beacon object to ContentValues. Used to save note to the database.
     * @return Beacon as ContentValues
     */
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(3);
        values.put("_id", beaconId);
        values.put("longzone", location.getLongZone());
        values.put("latzone", location.getLatZone());
        values.put("easting", location.getEasting());
        values.put("northing", location.getNorthing());
        values.put("altitude", location.getAltitude());
        return values;
    }

    public String toString() {
        return "Beacon " + (beaconId != null ? beaconId : "(no id provided)") + ": (" + location + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beacon beacon = (Beacon) o;
        return beaconId.equals(beacon.beaconId) && location.equals(beacon.location);
    }

    @Override
    public int hashCode() {
        return beaconId.hashCode() + location.hashCode();
    }
}
