package com.sillywalks.api.types;

import android.content.ContentValues;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;

/**
 * Represents a Note
 */
public class Note {
    private int id;
    private Calendar timestamp;
    private UTMCoordinates position;
    private String message;

    /**
     * Constructor with minimal metadata
     * @param position location where note was created
     * @param message message of note
     */
    public Note(UTMCoordinates position, String message) {
        this(0, GregorianCalendar.getInstance(), position, message);
    }

    /**
     * Constructor with timestamp as Calendar
     * @param id ID of note
     * @param timestamp time when note was created
     * @param position location where note was created
     * @param message message of note
     */
    public Note(int id, Calendar timestamp, UTMCoordinates position, String message) {
        this.id = id;
        this.timestamp = timestamp;
        this.position = position;
        this.message = message;
    }

    /**
     * Constructor with timestamp as string
     * @param id ID of note
     * @param timestamp time when note was created
     * @param position location where note was created
     * @param message message of note
     */
    public Note(int id, String timestamp, UTMCoordinates position, String message) {
        this.id = id;
        this.position = position;
        this.message = message;
        this.timestamp = new GregorianCalendar();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
        try {
            this.timestamp.setTime(dateFormat.parse(timestamp));
        } catch (ParseException e) {
            this.timestamp = GregorianCalendar.getInstance();
        }
    }

    /**
     * Gets the ID of the note
     * @return note id
     */
    public int getId() {
        return id;
    }

    public UTMCoordinates getPosition() {
        return position;
    }

    /**
     * Gets the position where the note was created
     * @return note location
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the time when the note was created
     * @return note timestamp
     */
    public Calendar getTimestamp() {
        return timestamp;
    }

    /**
     * Gets the Note timestamp as a string
     * @return timestamp as string
     */
    private String getTimestampString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
        return dateFormat.format(timestamp.getTime());
    }

    /**
     * Converts the Note object to ContentValues. Used to save note to the database.
     * @return Note as ContentValues
     */
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put("timestamp", getTimestampString());
        values.put("longzone", position.getLongZone());
        values.put("latzone", position.getLatZone());
        values.put("easting", position.getEasting());
        values.put("northing", position.getNorthing());
        values.put("altitude", position.getAltitude());
        values.put("accuracy", position.getAccuracy());
        values.put("satellites", position.getSatellitesUsedForFix());
        values.put("message", message);
        return values;
    }

    /**
     * Converts the Note object to a string array. Used to convert note to CSV.
     * @return Note as string array
     */
    public String[] toStringArray() {
        LinkedList<String> values = new LinkedList<>();
        values.addLast(getTimestampString());
        values.addLast(String.valueOf(position.getLongZone()));
        values.addLast(position.getLatZone());
        values.addLast(String.valueOf(position.getEasting()));
        values.addLast(String.valueOf(position.getNorthing()));
        values.addLast(message);
        String[] array = new String[values.size()];
        values.toArray(array);
        return array;
    }

    @Override
    public String toString() {
        return "Note{" +
                "timestamp=" + getTimestampString() +
                ", longzone=" + getPosition().getLongZone() +
                ", latzone=" + getPosition().getLatZone() +
                ", northing=" + getPosition().getEasting() +
                ", easting=" + getPosition().getNorthing() +
                ", message='" + getMessage() + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return id == note.id &&
                timestamp.equals(note.timestamp) &&
                position.equals(note.position) &&
                message.equals(note.message);
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(id).hashCode() + timestamp.hashCode() + position.hashCode() + message.hashCode();
    }

    /**
     * Returns an array of the database fields of note
     * @return array of DB fields
     */
    public static String[] getFieldsStringArray() {
        return new String[]{"timestamp", "longzone", "latzone", "easting", "northing", "message"};
    }
}
