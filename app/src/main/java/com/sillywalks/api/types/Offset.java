package com.sillywalks.api.types;

/**
 * Represents an offset
 */
public class Offset {

    private double easting;
    private double northing;
    private Double altitude;

    /**
     * Constructor for setting offset directly
     * @param easting offset in easting
     * @param northing offset in northing
     */
    public Offset(double easting, double northing) {
        this.easting = easting;
        this.northing = northing;
        this.altitude = null;
    }

    /**
     * Constructor for offset between two points
     * @param scannedBeacon Location from Beacon scan
     * @param phoneGps Location from GPS measurement
     */
    public Offset(UTMCoordinates scannedBeacon, UTMCoordinates phoneGps) {
        // ToDo: check if same latZone, longZone - VERY IMPORTANT, otherwise it cannot be directly subtracted...
        this.easting = scannedBeacon.getEasting() - phoneGps.getEasting();
        this.northing = scannedBeacon.getNorthing() - phoneGps.getNorthing();
        if (scannedBeacon.getAltitude() != null) {
            this.altitude = scannedBeacon.getAltitude() - phoneGps.getAltitude();
        }
    }

    /**
     * Gets the offset in easting
     * @return offset of easting
     */
    public double getEasting() {
        return easting;
    }

    /**
     * Gets the offset in northing
     * @return offset of northing
     */
    public double getNorthing() {
        return northing;
    }

    /**
     * Gets offset in meters
     * @return offset in meters
     */
    public double getInMeters() {
        return Math.sqrt(Math.pow(getEasting(), 2) + Math.pow(getNorthing(), 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offset that = (Offset) o;
        return that.easting == easting && that.northing == northing && that.altitude.equals(altitude);
    }
}
