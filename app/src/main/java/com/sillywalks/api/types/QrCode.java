package com.sillywalks.api.types;

import android.util.Log;

import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the result of a QR code scan
 */
public class QrCode {

    private JSONObject object;
    private JSONArray array;

    private Beacon beacon;
    private List<Beacon> beaconList;

    private boolean clearList;

    // JSON Node names
    private static final String LONGZONE = "longzone";
    private static final String LATZONE = "latzone";
    private static final String EASTING = "easting";
    private static final String NORTHING = "northing";
    private static final String ALTITUDE = "altitude";
    private static final String ID = "id";

    private static final String CLEAR = "clear";

    /**
     * Constructor for QrCode
     * @param rawResult Result of QR code scan
     */
    public QrCode(Result rawResult) {
        // try to pass incoming result as json object or array
        this.object = null;
        this.array = null;

        this.clearList = false;
        this.beacon = null;
        // init beacon list
        this.beaconList = new ArrayList<>();

        String resultString = rawResult.getText();
        parseJsonResult(resultString);
    }

    /**
     * Parse the JSON result from QR code scan
     * @param resultString JSON String from QR code scan
     */
    private void parseJsonResult(String resultString) {

        // ToDo: improve shitty try-catch block and repetitive code - perhaps try a pattern matcher to determine if Object or Array first?

        // try to parse to JSON Object
        try {
            object = new JSONObject(resultString);
            Log.d("DLR","QrCode: Scanned single beacon as reference point");

            if (object.has(CLEAR)) {
                clearList = object.getBoolean(CLEAR);
                Log.d("DLR","QrCode: Beacon list cleared");
            }

            if (object.has(LONGZONE) && object.has(LATZONE) && object.has(EASTING) && object.has(NORTHING)) {
                int longzone = object.getInt(LONGZONE);
                String latzone = object.getString(LATZONE);
                double easting = Double.parseDouble(object.getString(EASTING));
                double northing = Double.parseDouble(object.getString(NORTHING));

                Double altitude;
                if (object.has(ALTITUDE)) {
                    altitude = Double.parseDouble((object.getString(ALTITUDE)));
                }
                else {
                    altitude = null;
                }

                UTMCoordinates beaconLocation = new UTMCoordinates(longzone, latzone, easting, northing);
                beaconLocation.setAltitude(altitude);

                if (object.has(ID)) {
                    beacon = new Beacon(object.getString(ID), beaconLocation);
                } else {
                    beacon = new Beacon(beaconLocation);
                }
                Log.d("DLR", "QrCode: Single beacon: " + beacon.toString());
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.d("DLR", "QrCode.JSONException during Object parsing: " + e.toString());
        }

        // if not possible try to parse to JSON Array
        if (object == null) {
            try {
                array = new JSONArray(resultString);
                Log.d("DLR","QrCode: Scanned list of beacons");

                // fill List<Beacon> with beacons
                for (int i = 0; i < array.length();i++) {

                    // retrieve JSON Object from Array
                    JSONObject jsonObject = array.getJSONObject(i);

                    if (jsonObject.has(CLEAR)) {
                        clearList = jsonObject.getBoolean(CLEAR);
                    }
                    else if (jsonObject.has(LONGZONE) && jsonObject.has(LATZONE) && jsonObject.has(EASTING) && jsonObject.has(NORTHING)) {

                        int longzone = jsonObject.getInt(LONGZONE);
                        String latzone = jsonObject.getString(LATZONE);
                        double easting = Double.parseDouble(jsonObject.getString(EASTING));
                        double northing = Double.parseDouble(jsonObject.getString(NORTHING));

                        Double altitude;
                        if (jsonObject.has(ALTITUDE)) {
                            altitude = Double.parseDouble((jsonObject.getString(ALTITUDE)));
                        }
                        else {
                            altitude = null;
                        }

                        UTMCoordinates beaconLocation = new UTMCoordinates(longzone, latzone, easting, northing);
                        beaconLocation.setAltitude(altitude);

                        // create Beacon and get ID if available
                        Beacon b;

                        if (jsonObject.has(ID)) {
                            b = new Beacon(jsonObject.getString(ID), beaconLocation);
                        } else {
                            b = new Beacon(beaconLocation);
                        }
                        Log.d("DLR", "QrCode.beaconList.add(): " + b.toString());

                        // add Beacon to list
                        beaconList.add(b);
                    }
                }
                Log.d("DLR", "QrCode.beaconList.size(): " + beaconList.size());
            }
            catch (JSONException e) {
                e.printStackTrace();
                Log.d("DLR", "QrCode.JSONException during Array parsing: " + e.toString());
            }
        }
    }

    /**
     * Returns the Beacon the QR code represents
     * @return Beacon, null if not included in QR code
     */
    public Beacon getBeacon() {
            return this.beacon;
    }

    public List<Beacon> getBeaconList() {
        return this.beaconList;
    }

    /**
     * Gets whether the QR code scan contains a Beacon
     * @return scan contains Beacon
     */
    public boolean containsBeacon() {
        return (this.beacon != null);
    }

    /**
     * Gets whether the QR code scan contains a list of Beacons
     * @return scan has list of Beacons
     */
    public boolean containsBeaconList() {
        return !beaconList.isEmpty();
    }

    /**
     * Gets whether the QR code scan includes an instruction to clear the Beacon list
     * @return should clear Beacon list
     */
    public boolean containsClearAction() {
        return this.clearList;
    }



}
