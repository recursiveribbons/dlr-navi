package com.sillywalks.api.types;

import android.support.annotation.Nullable;

import java.util.Calendar;

/**
 * Represents UTM coordinates and GPS metadata
 */
public class UTMCoordinates {

    private Calendar timestamp;
    private int longZone;
    private String latZone;
    private double easting; // display on Overview as (int)
    private double northing; // display on Overview as (int)
    private Double altitude;

    private int satellitesUsedForFix;
    private double accuracy;


    public UTMCoordinates(int longZone, String latZone, double easting, double northing) {
        this.longZone = longZone;
        this.latZone = latZone;
        this.easting = easting;
        this.northing = northing;
        this.timestamp = Calendar.getInstance();
        this.satellitesUsedForFix = 0;
        this.accuracy = 0.0;
    }

    /**
     * Returns the measured altitude
     * @return Altitude. Can be null if not set.
     */
    @Nullable
    public Double getAltitude() {
        return altitude;
    }

    /**
     * Sets the measured altitude
     * @param altitude Measured altitude from GPS
     */
    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    /**
     * Returns the UTM zone
     * @return UTM zone
     */
    public int getLongZone() {
        return longZone;
    }

    /**
     * Returns the latitude band
     * @return latitude band
     */
    public String getLatZone() {
        return latZone;
    }

    /**
     * Gets the easting
     * @return easting
     */
    public double getEasting() {
        return easting;
    }

    /**
     * Gets the northing
     * @return northing
     */
    public double getNorthing() {
        return northing;
    }

    /**
     * Gets the time of GPS position measurement
     * @return Time of measurement
     */
    public Calendar getTimestamp() {
        return timestamp;
    }

    public UTMCoordinates addOffsetAndReturnCoordinates(Offset offset) {
        // ToDo: check if same latZone and longZone

        UTMCoordinates utmCoordinates = new UTMCoordinates(longZone, latZone, easting + offset.getEasting(), northing + offset.getNorthing());
        /*
         * Does offset make sense for altitude? Altitude randomly jumps 20-30 meters, so calculating an offset doesnt really make sense..
         *  utmCoordinates.setAltitude(altitude + (offset.getAltitude() != null ? offset.getAltitude() : 0));
         */
        utmCoordinates.setAltitude(altitude);
        return utmCoordinates;
    }

    /**
     * Determines the location to another point
     * @param other Location to compare to
     * @return distance to other location
     */
    public double getDistanceTo(UTMCoordinates other) {
        // ToDo: check if same latZone, longZone (!)
        return Math.sqrt(Math.pow(getEasting() - other.getEasting(), 2) + Math.pow(getNorthing() - other.getNorthing(), 2));
    }

    @Override
    public String toString() {
        return longZone + " " + latZone + " " + ((int) easting) + " " + ((int) northing + " " +  (altitude != null ? (int) (double) altitude : ""));
    }

    /**
     * Sets the number of satellites used for GPS fix
     * @param satellites number of satellites
     */
    public void setSatellitesUsedForFix(int satellites) {
        this.satellitesUsedForFix = satellites;
    }

    /**
     * Gets the number of satellites used for GPS fix
     * @return number of satellites
     */
    public int getSatellitesUsedForFix() {
        return this.satellitesUsedForFix;
    }

    /**
     * Gets the accuracy of GPS measurement
     * @return accuracy of GPS measurement
     */
    public double getAccuracy() {
        return accuracy;
    }

    /**
     * Sets the accuracy of GPS measurement
     * @param accuracy accuracy of GPS measurement
     */
    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UTMCoordinates that = (UTMCoordinates) o;

        if ((that.getAltitude() == null && getAltitude() != null) ||
                (that.getAltitude() != null && getAltitude() == null) &&
                        (that.getAltitude() != null && getAltitude() != null && !getAltitude().equals(that.getAltitude()))) {
            return false;
        }
        return that.getLongZone() == getLongZone() &&
                that.getLatZone().equals(getLatZone())&&
                that.getEasting() == getEasting()&&
                that.getNorthing() == getNorthing();
    }

}
