package com.sillywalks.naviapp;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.sillywalks.api.BeaconManager;
import com.sillywalks.api.CoordinateConversion;
import com.sillywalks.api.types.Beacon;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.layer.GroupLayer;
import org.mapsforge.map.layer.Layer;
import org.mapsforge.map.layer.overlay.Marker;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * A {@link Layer} implementation to display the list of Beacons on the map.
 */
public class BeaconOverlay extends GroupLayer implements Observer {

    private Bitmap bitmap;

    BeaconOverlay(Context context) {
        super();
        Resources resources = context.getResources();
        bitmap = AndroidGraphicFactory.convertToBitmap(resources.getDrawable(R.drawable.ic_maps_indicator_current_position));
        setBeacons(BeaconManager.getInstance(context).getBeacons());
    }

    private void setBeacons(List<Beacon> beacons) {
        Log.d(getClass().getName(), String.valueOf(beacons.size()));
        synchronized (this) {
            layers.clear();
            CoordinateConversion converter = new CoordinateConversion();
            for (Beacon beacon : beacons) {
                Log.d(getClass().getName(), beacon.toString());
                layers.add(new Marker(converter.utm2LatLong(beacon.getLocation()), bitmap, 0, 0));
            }
            requestRedraw();
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof BeaconManager) {
            BeaconManager manager = (BeaconManager)observable;
            setBeacons(manager.getBeacons());
        }
    }
}