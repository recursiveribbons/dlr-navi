package com.sillywalks.naviapp;

import android.content.Context;
import android.content.res.Resources;

import com.sillywalks.api.CoordinateConversion;
import com.sillywalks.api.CurrentMeasurements;
import com.sillywalks.api.GPSCorrector;
import com.sillywalks.api.types.UTMCoordinates;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.graphics.Canvas;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.layer.Layer;
import org.mapsforge.map.layer.overlay.Circle;
import org.mapsforge.map.layer.overlay.Marker;

import java.util.Observable;
import java.util.Observer;

/**
 * A {@link Layer} implementation to display the measured and corrected locations on the map.
 */
public class CurrentLocationOverlay extends Layer implements Observer {

    private final Circle circle;
    private final Marker device;
    private final Marker marker;

    CurrentLocationOverlay(Context context) {
        super();
        GPSCorrector.getInstance(context).subscribeToCurrentMeasurements(this);
        Resources resources = context.getResources();
        Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(resources.getDrawable(R.drawable.ic_maps_indicator_current_position));
        Bitmap bitmap2 = AndroidGraphicFactory.convertToBitmap(resources.getDrawable(R.drawable.location_dot));
        circle = new Circle(null, 0, null, null, true);
        marker = new Marker(new LatLong(0, 0), bitmap, 0, 0);
        device = new Marker(new LatLong(0, 0), bitmap2, 0, 0);
    }

    @Override
    public synchronized void draw(BoundingBox boundingBox, byte zoomLevel, Canvas canvas, Point topLeftPoint) {
        if (this.circle != null) {
            this.circle.draw(boundingBox, zoomLevel, canvas, topLeftPoint);
        }
        this.marker.draw(boundingBox, zoomLevel, canvas, topLeftPoint);
        this.device.draw(boundingBox, zoomLevel, canvas, topLeftPoint);
    }

    @Override
    protected void onAdd() {
        if (this.circle != null) {
            this.circle.setDisplayModel(this.displayModel);
        }
        this.marker.setDisplayModel(this.displayModel);
        this.device.setDisplayModel(this.displayModel);
    }

    @Override
    public void onDestroy() {
        this.circle.onDestroy();
        this.marker.onDestroy();
        this.device.onDestroy();
    }

    private void setPosition(UTMCoordinates corrected, UTMCoordinates measured, float accuracy) {
        synchronized (this) {
            CoordinateConversion converter = new CoordinateConversion();
            LatLong latLong = converter.utm2LatLong(corrected);
            this.marker.setLatLong(latLong);
            LatLong latLong2 = converter.utm2LatLong(measured);
            this.device.setLatLong(latLong2);
            if (this.circle != null) {
                this.circle.setLatLong(latLong);
                this.circle.setRadius(accuracy);
            }
            requestRedraw();
        }
    }

    /**
     * Updates the position markers
     * @param observable Observable of type CurrentMeasurements
     * @param o Parameters for Observable, ignored.
     */
    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof CurrentMeasurements) {
            CurrentMeasurements measurements = (CurrentMeasurements)observable;
            setPosition(measurements.getCorrectedLocation(), measurements.getPhoneGpsLocation(), (float)measurements.getAccuracyOfGPSFix());
        }
    }
}