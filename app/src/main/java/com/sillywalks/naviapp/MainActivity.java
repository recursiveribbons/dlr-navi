package com.sillywalks.naviapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import com.sillywalks.api.CSVHandler;
import com.sillywalks.api.GPSCorrector;
import com.sillywalks.api.CSVHandler;
import com.sillywalks.api.NoteManager;
import com.sillywalks.api.types.Note;

import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Class<?> mClss;
    private static final int ZXING_CAMERA_PERMISSION = 1;

    private static final String TAG = "DLR";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "MainActivity.onCreate()");

        AndroidGraphicFactory.createInstance(getApplication()); // relevant for MapsForge usage
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(SectionsPagerAdapter.OVERVIEW_FRAGMENT_POSITION); // Open ViewPager on OverviewFragment

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    /**
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
     * @param item MenuItem
     * @return Action success
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menuExportToCSV) {
            exportNotesAsCSV();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void exportNotesAsCSV() {
        CSVHandler csvHandler = new CSVHandler(this);
        String csvNotes = csvHandler.exportNotes();
        Log.d("DLR", "Notes loaded as CSV");

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/csv");
        i.putExtra(Intent.EXTRA_TEXT, csvNotes);

        try {
            startActivity(Intent.createChooser(i, "Send notes as CSV file"));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(mClss != null) {
                        Intent intent = new Intent(this, mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onDestroy() {
        /*
         * Whenever your activity exits, some cleanup operations have to be performed lest your app
         * runs out of memory.
         */
        //mapView.destroyAll();
        AndroidGraphicFactory.clearResourceMemoryCache();
        Log.d(TAG, "MainActivity.onDestroy()");
        stopLocationUpdates();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "MainActivity.onPause()");
        stopLocationUpdates();
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "MainActivity.onStop()");
        stopLocationUpdates();
        super.onStop();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "MainActivity.onResume()");
        startLocationUpdates();
        super.onResume();
    }

    private void stopLocationUpdates() {
        GPSCorrector.getInstance(this).stopLocationUpdates();
    }

    private void startLocationUpdates() {
        GPSCorrector.getInstance(this).startLocationUpdates();
    }

}
