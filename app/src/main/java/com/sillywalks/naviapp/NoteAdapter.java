package com.sillywalks.naviapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sillywalks.api.types.Note;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by krunz on 06.05.2018.
 */

public class NoteAdapter extends ArrayAdapter<Note> {

    private LayoutInflater inflater;

    public NoteAdapter(@NonNull Context context, @NonNull List<Note> objects) {
        super(context, R.layout.note_row, objects);
        inflater = LayoutInflater.from(context);
    }

    // Holder class for the inflated elements
    private static class ViewHolder {
        TextView timestamp, location, content;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            // inflate the layout
            convertView = inflater.inflate(R.layout.note_row, parent, false);

            // create holder
            holder = new ViewHolder();
            holder.timestamp = (TextView) convertView.findViewById(R.id.timestamp);
            holder.location = (TextView) convertView.findViewById(R.id.location);
            holder.content = (TextView) convertView.findViewById(R.id.content);

            convertView.setTag(holder);
        }

        else {
            // Holder already exists
            holder = (ViewHolder) convertView.getTag();
        }

        Note element = getItem(position);

        // Formatting the timestamp needs to be done here, if we're using Dates and not Strings

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN);

        holder.timestamp.setText(dateFormat.format(element.getTimestamp().getTime()));
        holder.location.setText(element.getPosition().toString());
        holder.content.setText(element.getMessage());

        return convertView;
    }
}
