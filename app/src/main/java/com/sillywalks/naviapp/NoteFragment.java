package com.sillywalks.naviapp;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import com.sillywalks.api.GPSCorrector;
import com.sillywalks.api.NoteManager;
import com.sillywalks.api.types.Note;
import com.sillywalks.api.types.UTMCoordinates;


public class NoteFragment extends ListFragment {

    private Button addNoteButton;
    private Button clearNotesButton;
    private EditText userInput;
    private NoteAdapter noteAdapter;

    private InputMethodManager imm;

    private static final String TAG = "DLR";


    public NoteFragment() {
        // Required empty public constructor
    }

    public static NoteFragment newInstance() {
        NoteFragment fragment = new NoteFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            Log.d(TAG, "NoteFragment.onCreateView()");
        View view = inflater.inflate(R.layout.fragment_note_list, container, false);

        addListenerToButtons(view, getActivity());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "NoteFragment.onActivityCreated()");

        NoteManager noteManager = new NoteManager(this.getContext());
        List<Note> notes = noteManager.getNotes();
        noteAdapter = new NoteAdapter(getContext(), notes);
        setListAdapter(noteAdapter);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        Log.d(TAG, "NoteFragment. Item " + position + " pressed");

        // input click listener afer setting list adapter
    }

    private void showKeyboard(View view) {
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboard(View view) {
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void addListenerToButtons(View view, final Context context) {

        addNoteButton = (Button) view.findViewById(R.id.buttonAddNote);
        clearNotesButton = (Button) view.findViewById(R.id.buttonClearNotes);

        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                View dialogView = li.inflate(R.layout.dialog_note, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setView(dialogView);

                userInput = (EditText) dialogView.findViewById(R.id.editTextNoteDialog);
                showKeyboard(userInput);

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {

                                        String noteMessage = userInput.getText().toString();

                                        // only close dialog if a message was entered
                                        if (!noteMessage.equals("")) {
                                            NoteManager db = new NoteManager(context);

                                            GPSCorrector gps = GPSCorrector.getInstance(context);
                                            UTMCoordinates location = gps.getCorrectedPosition();

                                            if(location == null) {
                                                hideKeyboard(userInput);
                                                dialog.cancel();
                                                Toast.makeText(getActivity(), "No location set, cannot save note.", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                            Note note = new Note(location, noteMessage);
                                            Note n = db.saveNote(note);
                                            noteAdapter.insert(n,0); // insert at top position

                                            hideKeyboard(userInput);
                                            dialog.cancel();
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        hideKeyboard(userInput);
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        clearNotesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.app.AlertDialog.Builder(context)
                        .setTitle("Warning")
                        .setMessage("Do you really wish to delete all notes?\nThis cannot be undone.")
                        .setPositiveButton("Delete all notes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface d, int id) {
                                NoteManager db = new NoteManager(context);
                                db.clearNotes();
                                noteAdapter.clear();
                                Log.d(TAG, "NoteFragment. All notes cleared");
                            }

                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface d, int id) {
                                // Aktion abbrechen, also nichts
                            }

                        })
                        .show();
            }
        });

    }


}
