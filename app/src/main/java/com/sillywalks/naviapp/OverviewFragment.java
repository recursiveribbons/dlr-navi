package com.sillywalks.naviapp;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sillywalks.api.CurrentMeasurements;
import com.sillywalks.api.GPSCorrector;
import com.sillywalks.api.types.UTMCoordinates;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class OverviewFragment extends Fragment implements Observer {

    private static final String TAG = "DLR";

    private Button buttonScanQr;
    private Context CONTEXT;

    private ZXingScannerView mScannerView;

    private TextView txPlanquadrat;
    private TextView txEasting;
    private TextView txNorthing;
    private TextView txAltitude;

    private TextView txConnectedSatellites;
    private TextView txAccuracy;

    private TextView txOffsetInMeters;
    private TextView txTimeSinceCalibration;
    private TextView txDistanceToQrBeacon;



    public OverviewFragment() {
        // Required empty public constructor
    }

    public static OverviewFragment newInstance(Context context) {
        return new OverviewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "OverviewFragment.onCreateView");

        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        txPlanquadrat = (TextView) view.findViewById(R.id.txPlanquadrat);
        txEasting = (TextView) view.findViewById(R.id.txEasting);
        txNorthing = (TextView) view.findViewById(R.id.txNorthing);
        txAltitude = (TextView) view.findViewById(R.id.txAltitude);

        txConnectedSatellites = (TextView) view.findViewById(R.id.txAmountSatellites);
        txAccuracy = (TextView) view.findViewById(R.id.txAccuracy);

        txOffsetInMeters = (TextView) view.findViewById(R.id.txOffsetInMeters);
        txTimeSinceCalibration = (TextView) view.findViewById(R.id.txCalibrationTime);
        txDistanceToQrBeacon = (TextView) view.findViewById(R.id.txDistance);


        GPSCorrector.getInstance(getContext()).subscribeToCurrentMeasurements(this);
        txConnectedSatellites.setText(getResources().getString(R.string.waiting_for_signal));
        return view;
    }

    private void setTxCoordinates(UTMCoordinates currentUTM) {
        String planquadrat = String.valueOf(currentUTM.getLongZone()) + " " + String.valueOf(currentUTM.getLatZone());
        txPlanquadrat.setText(planquadrat);

        // display 1 decimal, so user can see that movement is happening
        double truncatedEasting = truncateTo(currentUTM.getEasting(),2);
        double truncatedNorthing = truncateTo(currentUTM.getNorthing(),2);

        String formattedEasting = NumberFormat.getNumberInstance().format(truncatedEasting);
        String formattedNorthing = NumberFormat.getNumberInstance().format(truncatedNorthing);

        txEasting.setText(formattedEasting);
        txNorthing.setText(formattedNorthing);

        if (currentUTM.getAltitude() != null) {
            double truncatedAltitude = truncateTo(currentUTM.getAltitude(),2);
            String formattedAltitude = formatMeters(truncatedAltitude);
            txAltitude.setText(formattedAltitude);
        }

    }

    private static double truncateTo( double unroundedNumber, int decimalPlaces ){
        int truncatedNumberInt = (int)( unroundedNumber * Math.pow( 10, decimalPlaces ) );
        double truncatedNumber = (double)( truncatedNumberInt / Math.pow( 10, decimalPlaces ) );
        return truncatedNumber;
    }


    private void setTxOffset(double offsetInMeters) {
        String formattedOffset = formatMeters(offsetInMeters);
        txOffsetInMeters.setText(formattedOffset);
    }

    private void setDistanceToLastBeacon(Double distanceToBeacon) {
        if (distanceToBeacon != null) {
            String formattedDistance = formatMeters(distanceToBeacon);
            txDistanceToQrBeacon.setText(formattedDistance);
        }
    }

    private void setGPSFixInfo(int satelliteAmount, double accuracy) {
        txConnectedSatellites.setText(String.valueOf(satelliteAmount));
        txAccuracy.setText("+- " + formatMeters(accuracy));
    }

    private void setTimeSinceCalibration(Calendar calendarSinceCalibration) {
        if (calendarSinceCalibration != null) {
            long difference = Calendar.getInstance().getTimeInMillis() - calendarSinceCalibration.getTimeInMillis();

            long hours = TimeUnit.MILLISECONDS.toHours(difference);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(difference) % TimeUnit.HOURS.toMinutes(1);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(difference) % TimeUnit.MINUTES.toSeconds(1);

            String time = (hours > 0 ? hours + "hour" + (hours > 1 ? "s " : " "): "") + (minutes > 0 ? minutes + " min " : "") + (seconds > 0 ? seconds + " sec" : "");
            txTimeSinceCalibration.setText(time);
        }
    }

    private String formatMeters( double unroundedNumber ){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(unroundedNumber) + " m";
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable instanceof CurrentMeasurements) {
            CurrentMeasurements measurements = (CurrentMeasurements) observable;
            setTxCoordinates(measurements.getCorrectedLocation());

            setGPSFixInfo(measurements.getNumberOfSatellitesOfGPSFix(), measurements.getAccuracyOfGPSFix());

            setTxOffset(measurements.getOffsetInMeters());
            setDistanceToLastBeacon(measurements.getDistanceToLastBeaconInMeters());
            setTimeSinceCalibration(measurements.getTimeOfLastCalibration());
            //Log.d("OverviewFragment", "Updated measurements");
        }
        else {
            Log.d("OverviewFragment", "Received invalid observable");
        }
    }



}
