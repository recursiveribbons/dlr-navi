package com.sillywalks.naviapp;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.Result;
import com.sillywalks.api.BeaconManager;
import com.sillywalks.api.GPSCorrector;
import com.sillywalks.api.types.Beacon;
import com.sillywalks.api.types.QrCode;

import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanQrFragment extends Fragment implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private ViewPager mViewPager;

    public static ScanQrFragment newInstance() {
        return new ScanQrFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mScannerView = new ZXingScannerView(getActivity());
        mViewPager = (ViewPager) container.findViewById(R.id.container);
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // ToDo: somehow check the Qr-Code for validity


        QrCode qr = new QrCode(rawResult);


        if (qr.containsClearAction()) {
            BeaconManager beaconManager = BeaconManager.getInstance(getActivity());
            beaconManager.clearBeacons();
            Toast.makeText(getActivity(), "You just cleared all beacons.", Toast.LENGTH_SHORT).show();
        }

        // Does the qr code contain a beacon as a reference point?
        if (qr.containsBeacon()) {
            updateGuiWithCoordinates(qr.getBeacon());

            // return to OverviewFragment after scanning a reference beacon
            mViewPager.setCurrentItem(SectionsPagerAdapter.OVERVIEW_FRAGMENT_POSITION, true);

            if (qr.getBeacon().getBeaconId() != null && !qr.containsClearAction()) {
                Toast.makeText(getActivity(), "You just scanned beacon:\n'" + qr.getBeacon().getBeaconId() + "'", Toast.LENGTH_LONG).show();
            }
        }
        else if (qr.containsBeaconList()) {
            BeaconManager beaconManager = BeaconManager.getInstance(getActivity());

            List<Beacon> beaconList = qr.getBeaconList();
            beaconManager.saveBeacons(beaconList);

            if (beaconList.size() == 0) {
                Toast.makeText(getActivity(), "You scanned a list of duplicates.\nNo new beacons have been added to the map.", Toast.LENGTH_SHORT).show();
            }
            else {
                // return to map fragment after scanning a list of beacons
                mViewPager.setCurrentItem(SectionsPagerAdapter.MAP_FRAGMENT_POSITION, true);
                Toast.makeText(getActivity(), "You just scanned a list of " + beaconList.size() + " beacons.\nThey have been added to the map.", Toast.LENGTH_SHORT).show();
            }

        }
        else if (!qr.containsClearAction()) {
            Toast.makeText(getActivity(), "The QR code you scanned did not contain any beacon information\nor was in not in the right format.", Toast.LENGTH_LONG).show();
        }


        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // If you would like to resume scanning, call this method below:
                mScannerView.resumeCameraPreview(ScanQrFragment.this);
            }
        }, 2000);

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }



    private void updateGuiWithCoordinates(Beacon beaconFromScan) {
        GPSCorrector gpsCorrector = GPSCorrector.getInstance(getActivity());
        boolean success = gpsCorrector.setKnownPosition(beaconFromScan.getLocation());
        if(!success) {
            Toast.makeText(getActivity(), "No current location, can not scan.", Toast.LENGTH_SHORT).show();
        }
    }



}
