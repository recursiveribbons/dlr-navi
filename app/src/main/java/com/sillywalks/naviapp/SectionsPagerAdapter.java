package com.sillywalks.naviapp;

/**
 * Created by krunz on 06.05.2018.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public static final int MAP_FRAGMENT_POSITION = 0;
    public static final int SCAN_QR_FRAGMENT_POSITION = 1;
    public static final int OVERVIEW_FRAGMENT_POSITION = 2;
    public static final int NOTE_FRAGMENT_POSITION = 3;

    public static final int NUMBER_FRAGMENTS = 4;


    public SectionsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        switch (position) {
            case MAP_FRAGMENT_POSITION:
                return MapsForgeFragment.newInstance();
            // return MapsForgeFragment.newInstance();
            case SCAN_QR_FRAGMENT_POSITION:
                return ScanQrFragment.newInstance();
            case OVERVIEW_FRAGMENT_POSITION:
                return OverviewFragment.newInstance(mContext);
            case NOTE_FRAGMENT_POSITION:
                return NoteFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUMBER_FRAGMENTS;         // Number of total pages shown
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case MAP_FRAGMENT_POSITION:
                return mContext.getResources().getString(R.string.fragment_map);
            case SCAN_QR_FRAGMENT_POSITION:
                return mContext.getResources().getString(R.string.fragment_qr);
            case OVERVIEW_FRAGMENT_POSITION:
                return mContext.getResources().getString(R.string.fragment_overview);
           case NOTE_FRAGMENT_POSITION:
                return mContext.getResources().getString(R.string.fragment_notes);
        }
        return null;
    }
}